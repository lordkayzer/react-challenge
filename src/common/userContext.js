import React, {useEffect, useState, useMemo, useContext, createContext} from 'react';
import api from './api';

const apiInstance = api();
const UsersContext = createContext([]);

export const UsersProvider = (props) => {
  const [users, setUsers] = useState([]);
  const [toDos, setToDos] = useState([]);

  useEffect(() => {
    (async () => {
      const {data} = await apiInstance.getUsers();
      setUsers(data);
    })();
  }, []);

  const selectUser = ({rows}) => {
    Promise.all(rows.map(({id}) => apiInstance.getToDo(id)))
      .then(t => {
        let todos = [];
        t.forEach(({data}) => todos = todos.concat(data));
        setToDos(todos);
      });
  }

  const value = useMemo(() => ({
    users,
    toDos,
    selectUser
  }), [users, toDos]);

  return <UsersContext.Provider value={value} {...props}/>;
}

export const useUsers = () => {
  const context = useContext(UsersContext);

  if (!context) {
    throw new Error('useUser needs to be inside UserContext');
  }

  return useContext(UsersContext);
}

import axios from 'axios';

export default () => {
  const instance = axios.create({
    baseURL: 'https://jsonplaceholder.typicode.com/',
    timeout: 1000,
    headers: {'X-Custom-Header': 'foobar'}
  });

  const getUsers = () => {
    return instance.get('users');
  }

  const getToDo = (userId) => {
    return instance.get(`todos?userId=${userId}`)
  }

  return {
    getUsers,
    getToDo
  }
}


import React from 'react';
import {BrowserRouter, Redirect, Route, Switch} from 'react-router-dom';
import DashBoardPage from "../pages/dashboard/DashBoardPage";
import UserPage from "../pages/dashboard/UsersPage";
import ToDoPage from "../pages/dashboard/ToDoPage";

const Router = () => {

  return (
    <BrowserRouter>
      <DashBoardPage>
        <Switch>
          <Route path="/users" component={UserPage}/>
          <Route path="/todos" component={ToDoPage}/>
          <Route render={() => <Redirect to="/users"/>}/>
        </Switch>
      </DashBoardPage>
    </BrowserRouter>
  );
};

export default Router;

import React from 'react';
import {NavLink} from 'react-router-dom';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import PlaylistAddCheck from '@material-ui/icons/PlaylistAddCheck';
import PeopleIcon from '@material-ui/icons/People';
import Divider from "@material-ui/core/Divider";
import List from "@material-ui/core/List";
import useStyles from "../../pages/dashboard/styleHomePage";

const NavBar = () => {
  const classes = useStyles();

  return <div className={classes.menu}>
    <Divider/>
    <List>
      <ListItem button component={NavLink} to='/users' activeClassName={classes.menuButtonSelected}>
        <ListItemIcon>
          <PeopleIcon/>
        </ListItemIcon>
        <ListItemText primary="Usuarios"/>
      </ListItem>
      <ListItem button component={NavLink} to='/todos' activeClassName={classes.menuButtonSelected}>
        <ListItemIcon>
          <PlaylistAddCheck/>
        </ListItemIcon>
        <ListItemText primary="To Do"/>
      </ListItem>
    </List>
    <Divider/>
  </div>
};

export default NavBar;

import React from 'react';
import TODOPaper from "./components/TODOPaper";
import {useUsers} from '../../common/userContext';

export default function ToDoPage() {
  const {toDos} = useUsers();
  return <TODOPaper todos={toDos}/>;
}

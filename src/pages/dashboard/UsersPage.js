import React from 'react';
import UsersPaper from "./components/UsersPaper";
import {useUsers} from '../../common/userContext';

export default function UserPage() {
  const {users, selectUser} = useUsers();
  return <UsersPaper users={users} onSelection={selectUser}/>;
}

import React from "react";
import {DataGrid} from '@material-ui/data-grid';
import useStyles from "../styleHomePage";

const columns = [
  {field: 'id', headerName: 'ID', width: 70},
  {field: 'username', headerName: 'Username', width: 130},
  {field: 'website', headerName: 'Website', width: 130},
  {field: 'company', headerName: 'Catchphrase', width: 300, 'valueGetter': (param) => param.data.company.catchPhrase},
];

const UsersPaper = ({users, onSelection}) => {
  const classes = useStyles();

  return <DataGrid
    rows={users}
    columns={columns}
    pageSize={10}
    checkboxSelection
    className={classes.dataGrid}
    onSelectionChange={onSelection}
  />
}

export default UsersPaper
